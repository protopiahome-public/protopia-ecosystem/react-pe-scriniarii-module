# React PE Scriniarii Module

![React](https://img.shields.io/badge/-React-black?style=flat-square&logo=react)
![HTML5](https://img.shields.io/badge/-HTML5-E34F26?style=flat-square&logo=html5&logoColor=white)
![CSS3](https://img.shields.io/badge/-CSS3-1572B6?style=flat-square&logo=css3)
![Bootstrap](https://img.shields.io/badge/-Bootstrap-563D7C?style=flat-square&logo=bootstrap)
![TypeScript](https://img.shields.io/badge/-TypeScript-007ACC?style=flat-square&logo=typescript)
![GraphQL](https://img.shields.io/badge/-GraphQL-E10098?style=flat-square&logo=graphql)
![Apollo GraphQL](https://img.shields.io/badge/-Apollo%20GraphQL-311C87?style=flat-square&logo=apollo-graphql)


<p align="center">
  <a href="https://nact.xyz/" target="blank"><img src="https://gitlab.com/uploads/-/system/project/avatar/54387363/pe_logo_38.jpg" width="100" alt="Scriniarii Logo" /></a>
</p> 

 PE-module for [ProtopiaEcosystem React Client](https://protopiahome-public.gitlab.io/protopia-ecosystem/cra-template-pe/).
 Includes RSS feed views.

## Includes

1. RSS-feeds content display View
 
## Availability

1. ProtopiaEcosystem-react-client v.>2.0 

## Manual installation and configuration 

1. Install new react client ProtopiaEcosystem. Or open an existing development

2. Start install module:
> `npm run addmodule react-pe-scriniarii-module https://gitlab.com/protopiahome-public/protopia-ecosystem/react-pe-scriniarii-module`

3. Wait for the installation to complete.

4. By default, all graphic panels and screen access menus are already registered. But if your client is configured in local or static content display mode, you can optionally change the location of the access buttons to these screens in the config/layout.jsion file

5. In the routes section of the configuration file layouts.json, we create ScriniariiView configurator in the selected menu (for example, in the site header - main), as shown in the example below:
<pre>{
    "id": "829e3415-3f27-46cb-b7d0-5fd788888",
    "title": "RSS feed",
    "description": "",
    "icon": "fas fa-rss",
    "route": "rss",
    "hide_title": false,
    "component": "ScriniariiView",
    "childRoutes": [ ],
    "helps": [ ],
    "extend_params": {
        "src": [
            "https://www.playground.ru/rss/articles.xml",
            "https://www.playground.ru/rss/news.xml",
            "https://lenta.ru/rss"
        ]
    },
    "is_left": 1,
    "module": "pe-scriniarii-module",
    "is_cabinet": true, 
    "thumbnail": "https://i.pinimg.com/564x/8d/d5/e5/8dd5e54e22bb28d5ddff8a60ba174427.jpg",
    "menu": "menu",
    "folder": 4
} </pre>

In the above example, we write the argument extend_params.src, separated by commas, the links to RSS that we want to display

6. Build the client 
> `npm run build`

7. deploy it on your server
