
import BasicState from "src/libs/basic-view"
import ScriniariiForm from "./scriniarii/ScriniariiForm"

export default class ScriniariiView extends BasicState {
    props: any
    addRender = () => {
        return <div className="d-flex w-100 m-0 position-relative">
            <ScriniariiForm {...this.props} />  
        </div>
    }
}