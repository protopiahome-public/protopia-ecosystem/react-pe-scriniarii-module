
import { ID, Icon, URL } from "@/libs/interfaces/layouts";
import { create } from "zustand";
import { createJSONStorage, devtools, persist } from "zustand/middleware";

export interface IScriniariiStore {
    /* It's example. Clear this stroke. */
    exampleToDelete: string
}

export const useScriniariiStore: any = create( 
    devtools( 
        persist<IScriniariiStore>( 
            (set: any, get: any ) => ({ 
                /* It's example. Clear this stroke. */
                exampleToDelete: ""
            }),
            {
                name: 'pe-scriniarii-storage', 
                storage: createJSONStorage( () => localStorage ), 
            } 
        )
    )
)
