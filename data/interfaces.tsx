import { Email, ID, URL, HTML, IMenuItem, POST_STATUS, IPost, Role } from "@/libs/interfaces/layouts"


export interface IRSSChannel {
    title: string
    description?: string
    link: URL
}
export interface IRSSEnclosure {
    url:URL
    length?: string
    type?: "image/jpeg" | "image/png" | "image/webp" 
}
export interface IRSSItem {
    title: string
    description?: HTML
    pubDate?: string
    guid?: string
    link: URL
    enclosure?: IRSSEnclosure
    channel?: IRSSChannel
}